<?php

/**
 * @file
 * uw_cfg_clone_web_pages.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_clone_web_pages_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'clone node'.
  $permissions['clone node'] = array(
    'name' => 'clone node',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'clone',
  );

  // Exported permission: 'clone own nodes'.
  $permissions['clone own nodes'] = array(
    'name' => 'clone own nodes',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'events editor' => 'events editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'clone',
  );

  return $permissions;
}
